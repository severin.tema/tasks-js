/** @format */
/*
--- Завдання 6
- Умова:
Створити програму, яка приймає числове значення, та виводить суму всіх чисел, 
що кратні 3 або 5 і менше введеного числа. Наприклад, для числа 10, сума чисел, 
що кратні 3 або 5, буде 3 + 5 + 6 + 9 = 23.

Програма повинна перевіряти введене користувачем число на коректність та повторно запитувати, 
якщо число введено неправильно.

- Приклад:
Введіть число: 15
Сума чисел, що кратні 3 або 5 та менше 15: 45
*/

let num;

while (isNaN(num) || num <= 0) {
  num = parseInt(prompt("Введіть число:"));
}

let full = 0;
for (let i = 0; i < num; i++) {
  if (i % 3 === 0 || i % 5 === 0) {
    full += i;
  }
}

console.log(`Сума чисел, що кратні 3 або 5 та менше ${num}: ${full}`);
