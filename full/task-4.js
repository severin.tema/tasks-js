/** @format */
/*
--- Завдання 4
- Умова:
Напишіть програму, яка знаходить найбільший спільний дільник (НСД) двох заданих користувачем чисел. 
Користувач має ввести два числа, а програма повинна вивести їх НСД.

- Приклад:
Введіть перше число: 12
Введіть друге число: 18

НСД чисел 12 та 18 дорівнює 6.
*/

let numberOne = parseInt(prompt("Введіть перше число:"));
let numberTwo = parseInt(prompt("Введіть друге число:"));
let res;

for (let i = 1; i <= numberOne && i <= numberTwo; i++) {
  if (numberOne % i === 0 && numberTwo % i === 0) {
    res = i;
  }
}

//   if (numberOne > numberTwo) {
//     numberOne = numberOne - numberTwo;
//   } else {
//     numberTwo = numberTwo - numberOne;
//   }

console.log(`НСД чисел ${numberOne} та ${numberTwo} дорівнює ${res}`);
