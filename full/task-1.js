/** @format */
/*
--- Завдання 1
- Умова:
Напишіть програму, яка запитує в користувача його вік та рік народження, 
а потім обчислює рік, коли йому виповниться 100 років. 
Програма повинна вивести цей рік та повідомлення про те, 
скільки років залишилося до 100-річчя користувача. 

- Приклад:
Введіть свій вік: 25
Введіть рік свого народження: 1998

Вам виповниться 100 років у 2098 році.
Залишилось ще 75 років до вашого 100-річчя.
*/

const age = Number(prompt("Введіть свій вік:"));
const year = Number(prompt("Введіть рік свого народження:"));

const cYear = new Date().getFullYear();
const plusYear = year + 100;
const res = plusYear - cYear;

console.log(res);

console.log(`Вам виповниться 100 років у ${plusYear} році`);
console.log(`Залишилось ще ${res} років до вашого 100-річчя`);
